﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloYourName
{
    class Program
    {
        static void Main(string[] args)
        {
            var FirstName = "James";
            var LastName = "Beck";
            var Greeting = "Hello good chum. What is your name?";
            var UserInput = "0";

            Console.WriteLine("Hello good chum. My name is {0} {1}", FirstName, LastName);
            Console.WriteLine("Hello good chum. My name is " + FirstName + " " + LastName);
            Console.WriteLine($"Hello good chum. My name is {FirstName} {LastName}");

            Console.WriteLine(Greeting);
            UserInput = Console.ReadLine();
            Console.WriteLine($"Thank you {UserInput}. Now I am sentient.");
        }
    }
}
